## Build the Docker Image

`git clone https://gitlab.com/gitlab-com/customer-success/solutions-architecture/strategic-field-org/compliance-testing-with-conftest`

`docker build -t conftest .`

## Using the Docker image

`docker run -it -v $PWD:/project conftest test deployment.yaml`
> `FAIL - deployment.yaml - main - Containers must not run as root`

## Testing a GitLab Pipeline file
`docker run -it -v $PWD:/project conftest test examples/gitlab-ci.yml`

## Extending this example
1. Create your own rego files in the policy folder as this is where conftest is looking for them
2. Create a configuration file to run against your policy

## More information
1. https://github.com/open-policy-agent/conftest
2. https://play.openpolicyagent.org/


