FROM golang:1.17.6-alpine as base
ARG ARCH=amd64
ARG VERSION
ARG COMMIT
ARG DATE
ENV GOOS=linux \
    CGO_ENABLED=0 \
    GOARCH=${ARCH}
RUN apk add --no-cache git
WORKDIR /app
RUN git clone https://github.com/open-policy-agent/conftest /app

RUN go mod download

COPY . .

## BUILDER STAGE ##
FROM base as builder
RUN go build -o conftest -ldflags="-w -s -X github.com/open-policy-agent/conftest/internal/commands.version=${VERSION}" main.go

## RELEASE ##
FROM alpine:3.15.0

# Install git for protocols that depend on it when using conftest pull
RUN apk add --no-cache git

COPY --from=builder /app/conftest /
RUN ln -s /conftest /usr/local/bin/conftest
WORKDIR /project

ENTRYPOINT ["/conftest"]
CMD ["--help"]